import 'package:flutter/material.dart';

class Food_Two extends StatelessWidget {
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: Text("Accessories"),
      ),
      body: Scaffold(
        body: Scrollbar(
          child: ListView(
            padding: const EdgeInsets.symmetric(vertical: 8),
            children: [
              ListTile(
                leading: ExcludeSemantics(
                  child: CircleAvatar(child: Text("C")),),
                title: Text("HUFT Collar Belt"),
                subtitle: Text("\$50.00"),),
              ListTile(
                leading: ExcludeSemantics(
                  child: CircleAvatar(child: Text("BN")),),
                title: Text("Dog Bed + Nest"),
                subtitle: Text("\$27.99"),),
              ListTile(
                leading: ExcludeSemantics(
                  child: CircleAvatar(child: Text("PS")),),
                title: Text("Pet Shoes"),
                subtitle: Text("\$40.99"),),
              ListTile(
                leading: ExcludeSemantics(
                  child: CircleAvatar(child: Text("HL")),),
                title: Text("Harness & Leash"),
                subtitle: Text("\$19.49"),),
              ListTile(
                leading: ExcludeSemantics(
                  child: CircleAvatar(child: Text("BB")),),
                title: Text("Basket Bed"),
                subtitle: Text("\$52.09"),
              ),
              ListTile(
                leading: ExcludeSemantics(
                  child: CircleAvatar(child: Text("TW")),
                ),
                title: Text("Taste of the Wild Ancient Prairie"),
                subtitle: Text("\$48.99"),
              ),
              ListTile(
                leading: ExcludeSemantics(
                  child: CircleAvatar(child: Text("LB")),
                ),
                title: Text("Leather Belt"),
                subtitle: Text("\$55.99"),),
            ],
          ),
        ),
      ),);
  }
}