import 'package:cs481_lab6/accessories.dart';
import 'package:cs481_lab6/food_screen_one().dart';
import 'package:cs481_lab6/toys.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'list.dart';

class ScreenTwo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white60,
      body: new Center(
        child: new Container(
          child: new Column(
            children: [
              Container(
                height: 300,
                width: 350,
                margin: EdgeInsets.all(10),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(5),
                  child: Image.asset('images/food.jpg',),
                ),
              ),
              Spacer(),
              Container(
                alignment: Alignment.centerLeft,
                child: Text("Diet should be appropriate to the dog,",style: TextStyle(fontFamily: 'Comfortaa', color: Colors.black54,fontSize: 20),),
              ),
              Container(
                alignment: Alignment.centerLeft,
                child: Text("Check the variety of dog foods and Accessories",style: TextStyle(fontFamily: 'Comfortaa', color: Colors.black54,fontSize: 20),),
              ),
              Spacer(),
              Container(
                child: new Row(
                  children: [
                    Spacer(),
                    RaisedButton(onPressed: ()
                    {
                      Navigator.of(context).push(PageRouteBuilder(
                          pageBuilder: (context, animation, anotherAnimation)
                          {
                            return Food_One();
                          },
                        transitionDuration: Duration(milliseconds: 2000),
                        transitionsBuilder: (context, animation, anotherAnimation, child)
                        {
                          animation = CurvedAnimation(
                              parent: animation, curve: curveList[9]);
                          return RotationTransition(
                              turns: animation,
                          child: child,);
                        }
                          ));
                    }, padding: EdgeInsets.all(10),color: Colors.black,child: Text("Foods", style: TextStyle(color: Colors.white, fontSize: 17),)),
                    Spacer(),
                    RaisedButton(onPressed: ()
                    {
                      Navigator.of(context).push(PageRouteBuilder(
                          pageBuilder: (context, animation, anotherAnimation)
                          {
                            return Food_Two();
                          },
                          transitionDuration: Duration(milliseconds: 2000),
                          transitionsBuilder: (context, animation, anotherAnimation, child)
                          {
                            animation = CurvedAnimation(
                                parent: animation, curve: curveList[4]);
                            return ScaleTransition(
                              scale: animation,
                              child: child,);
                          }
                      ));
                    }, padding: EdgeInsets.all(10),color: Colors.black,child: Text("Accessories", style: TextStyle(color: Colors.white, fontSize: 17),)),
                    Spacer(),
                    RaisedButton(onPressed: ()
                    {
                      Navigator.of(context).push(PageRouteBuilder(
                          pageBuilder: (context, animation, anotherAnimation)
                          {
                            return Food_Three();
                          },
                          transitionDuration: Duration(milliseconds: 2000),
                          transitionsBuilder: (context, animation, anotherAnimation, child)
                          {
                            animation = CurvedAnimation(
                                parent: animation, curve: curveList[4]);
                            return Align(
                              child: SizeTransition(
                                  sizeFactor: animation,
                              child: child,
                              axisAlignment: 0.0,));
                          }
                      ));
                    }, padding: EdgeInsets.all(10),color: Colors.black,child: Text("Containment", style: TextStyle(color: Colors.white, fontSize: 17),)),
                    Spacer()
                  ],
                ),
              ),
              Spacer(),
            ],
          ),
        ),

      ),
      //floating action to open container transform
      /* floatingActionButton: OpenContainer(
          closedBuilder: (_, openContainer){
            return FloatingActionButton(
              elevation: 0.0,
              onPressed: openContainer,
              backgroundColor: Colors.black,
              child: Icon(Icons.pets, color: Colors.white),
            );
          },
          openColor: Colors.blueGrey,
          closedElevation: 5.0,
          closedShape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(100)
          ),
          closedColor: Colors.blueGrey,
          //opening new screen here
          openBuilder: (_, closeContainer)
          {
            return Scaffold(
              appBar: AppBar(
                backgroundColor: Colors.black,
                title: Text("Which Dog Breeds you are looking for?"),
                leading: IconButton(
                  onPressed: closeContainer,
                  icon: Icon(Icons.arrow_back, color: Colors.white),
                ),
              ),
              body: Scrollbar(
                child: ListView(
                  padding: const EdgeInsets.symmetric(vertical: 8),
                  children: [
                    ListTile(
                      leading: ExcludeSemantics(
                        child: CircleAvatar(child: Text("A")),),
                      title: Text("Affenpinscher"),
                      subtitle: Text("Loyal, curious, and famously amusing"),),
                    ListTile(
                      leading: ExcludeSemantics(
                        child: CircleAvatar(child: Text("A")),),
                      title: Text("Airedale Terrier"),
                      subtitle: Text("The King of Terriers"),),
                    ListTile(
                      leading: ExcludeSemantics(
                        child: CircleAvatar(child: Text("B")),),
                      title: Text("Barbet"),
                      subtitle: Text("An archetypic water dog of France"),),
                    ListTile(
                      leading: ExcludeSemantics(
                        child: CircleAvatar(child: Text("B")),),
                      title: Text("Basenji"),
                      subtitle: Text("Africa’s Barkless Dog"),),
                    ListTile(
                      leading: ExcludeSemantics(
                        child: CircleAvatar(child: Text("C")),),
                      title: Text("Canaan Dog"),
                      subtitle: Text("The national dog of Israel"),
                    ),
                    ListTile(
                      leading: ExcludeSemantics(
                        child: CircleAvatar(child: Text("C")),
                      ),
                      title: Text("Cane Corso"),
                      subtitle: Text("Smart and Trainable"),
                    ),
                    ListTile(
                      leading: ExcludeSemantics(
                        child: CircleAvatar(child: Text("D")),
                      ),
                      title: Text("Doberman Pinscher"),
                      subtitle: Text("Sleek and Powerful"),),
                    ListTile(
                      leading: ExcludeSemantics(
                        child: CircleAvatar(child: Text("D")),
                      ),
                      title: Text("Dogo Argentino"),
                      subtitle: Text("A pack-hunting dog"),),
                    ListTile(
                      leading: ExcludeSemantics(
                        child: CircleAvatar(child: Text("E")),
                      ),
                      title: Text("Estrela Mountain Dog"),
                      subtitle: Text("An excellent livestock guardian"),),
                    ListTile(
                      leading: ExcludeSemantics(
                        child: CircleAvatar(child: Text("F")),
                      ),
                      title: Text("Field Spaniel"),
                      subtitle: Text("Sweet and Sensitive"),),
                    ListTile(
                      leading: ExcludeSemantics(
                        child: CircleAvatar(child: Text("F")),
                      ),
                      title: Text("Flat-Coated Retriever"),
                      subtitle: Text("Forever Young"),
                    ),
                  ],
                ),
              ),
            );
          }
      ),*/
    );
  }


}