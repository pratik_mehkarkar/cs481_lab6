import 'dart:io';
import 'package:animations/animations.dart';
import 'package:cs481_lab6/breeds.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'list.dart';

class ThirdScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white60,
      body: new Center(
        child: new Container(
          child: new Column(
            children: [
              Container(
                height: 300,
                width: 350,
                margin: EdgeInsets.all(10),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(5),
                  child: Image.asset('images/dog1.jpg',),
                ),
              ),
              Spacer(),
              Container(
                alignment: Alignment.centerLeft,
                child: Text("Let's Take a look into our dog store..,",style: TextStyle(fontFamily: 'Comfortaa', color: Colors.black54,fontSize: 20),),
              ),
              Spacer(),
              Container(
                child: new Row(
                  children: [
                    Spacer(),
                    RaisedButton(onPressed: ()
                    {
                      Navigator.of(context).push(PageRouteBuilder(
                          pageBuilder: (context, animation, anotherAnimation)
                          {
                            return Screen_Breed();
                          },
                          transitionDuration: Duration(milliseconds: 2000),
                          transitionsBuilder: (context, animation, anotherAnimation, child)
                          {
                            animation = CurvedAnimation(
                                parent: animation, curve: curveList[4]);
                            return FadeTransition(
                              opacity: animation,
                              child: child,);
                          }
                      ));
                    }, padding: EdgeInsets.all(10),
                        color: Colors.black,child: Text("Dog Gallery", style: TextStyle(color: Colors.white, fontSize: 20),)),
                    Spacer(),
                    RaisedButton(onPressed: ()
                    {
                      showModal(
                          context: context,
                          configuration: FadeScaleTransitionConfiguration(),
                          builder: (context){
                            return AlertDialog(
                              title: Text("Exit Modal"),
                              content: Text("Do you want to exit PuppyCare App?"),
                             actions: [
                               FlatButton(
                                 onPressed: () => exit(0),
                                 /*Navigator.of(context).pop(true)*/
                                 child: Text('Yes'),
                               ),
                               FlatButton(
                                 onPressed: () => Navigator.of(context).pop(false),
                                 child: Text('No'),
                               ),
                             ],
                            );
                          }
                      );
                    }, padding: EdgeInsets.all(10),
                        color: Colors.black,child: Text("Exit App", style: TextStyle(color: Colors.white, fontSize: 20),)),
                    Spacer()
                  ],
                ),
              ),
              Spacer(),
            ],
          ),
        ),
      ),
    );
  }


}