import 'package:flutter/material.dart';

class Food_Three extends StatelessWidget {
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: Text("Containment"),
      ),
      body: Scaffold(
        body: Scrollbar(
          child: ListView(
            padding: const EdgeInsets.symmetric(vertical: 8),
            children: [
              ListTile(
                leading: ExcludeSemantics(
                  child: CircleAvatar(child: Text("D")),),
                title: Text("Double Door wire Crate"),
                subtitle: Text("\$37.00"),
              ),
              ListTile(
                leading: ExcludeSemantics(
                  child: CircleAvatar(child: Text("DM")),),
                title: Text("Durable Mat Pad"),
                subtitle: Text("\$27.99"),),
              ListTile(
                leading: ExcludeSemantics(
                  child: CircleAvatar(child: Text("PC")),),
                title: Text("Deluxe Pet Carrier"),
                subtitle: Text("\$40.99"),),
              ListTile(
                leading: ExcludeSemantics(
                  child: CircleAvatar(child: Text("PC")),),
                title: Text("2-Door Top Pet Kennel"),
                subtitle: Text("\$19.49"),),
              ListTile(
                leading: ExcludeSemantics(
                  child: CircleAvatar(child: Text("EP")),),
                title: Text("Exercise Pen"),
                subtitle: Text("\$52.09"),
              ),
              ListTile(
                leading: ExcludeSemantics(
                  child: CircleAvatar(child: Text("PS")),
                ),
                title: Text("Booster Pet Seat"),
                subtitle: Text("\$48.99"),
              ),
              ListTile(
                leading: ExcludeSemantics(
                  child: CircleAvatar(child: Text("DC")),
                ),
                title: Text("Defender Pet Crate"),
                subtitle: Text("\$55.99"),),
            ],
          ),
        ),
      ),);
  }
}