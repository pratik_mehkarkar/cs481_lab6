import 'package:flutter/material.dart';

class Food_One extends StatelessWidget {
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.black,
          title: Text("Dog Food"),
        ),
      body: Scaffold(
        body: Scrollbar(
          child: ListView(
            padding: const EdgeInsets.symmetric(vertical: 8),
            children: [
              ListTile(
                leading: ExcludeSemantics(
                  child: CircleAvatar(child: Text("TF")),),
                title: Text("The Farmer's Dog"),
                subtitle: Text("\$60.00"),),
              ListTile(
                leading: ExcludeSemantics(
                  child: CircleAvatar(child: Text("IR")),),
                title: Text("Instinct RawBoost Mixers"),
                subtitle: Text("\$27.99"),),
              ListTile(
                leading: ExcludeSemantics(
                  child: CircleAvatar(child: Text("ZL")),),
                title: Text("Zignature Lamb Limited Canned Food"),
                subtitle: Text("\$40.99"),),
              ListTile(
                leading: ExcludeSemantics(
                  child: CircleAvatar(child: Text("FF")),),
                title: Text("Freshpet Fresh"),
                subtitle: Text("\$9.49"),),
              ListTile(
                leading: ExcludeSemantics(
                  child: CircleAvatar(child: Text("OC")),),
                title: Text("Organix Chicken & Oatmeal Dry"),
                subtitle: Text("\$52.09"),
              ),
              ListTile(
                leading: ExcludeSemantics(
                  child: CircleAvatar(child: Text("TW")),
                ),
                title: Text("Taste of the Wild Ancient Prairie"),
                subtitle: Text("\$48.99"),
              ),
              ListTile(
                leading: ExcludeSemantics(
                  child: CircleAvatar(child: Text("OO")),
                ),
                title: Text("Orijen Original Dry Food"),
                subtitle: Text("\$55.99"),),
              ListTile(
                leading: ExcludeSemantics(
                  child: CircleAvatar(child: Text("SF")),
                ),
                title: Text("Spot Farms Turkey & Whole Grain"),
                subtitle: Text("\$29.99"),),
              ListTile(
                leading: ExcludeSemantics(
                  child: CircleAvatar(child: Text("ON")),
                ),
                title: Text("Only Natural Pet MaxMeat"),
                subtitle: Text("\$25.99"),),
              ListTile(
                leading: ExcludeSemantics(
                  child: CircleAvatar(child: Text("RR")),
                ),
                title: Text("Rachael Ray Nutrish Supermedleys"),
                subtitle: Text("\$30.99"),),
              ListTile(
                leading: ExcludeSemantics(
                  child: CircleAvatar(child: Text("ST")),
                ),
                title: Text("Spot Farms Turkey"),
                subtitle: Text("\$34.00"),
              ),
            ],
          ),
        ),
      ),);
  }
}