import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class ScreenOne extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white60,
      body: new Center(
        child: new Container(
          child: new Column(
            children: [
              Container(
                width: 130,
                margin: EdgeInsets.all(10),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(5),
                  child: Image.asset(
                    'images/puppy.jpg', height: 200, width: 350,),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 30),
                alignment: Alignment.centerLeft,
                child: Text("HELLO",style: TextStyle(fontFamily: 'mura', color: Colors.black,fontSize: 50),),
              ),
              Container(
                alignment: Alignment.centerLeft,
                child: Text("HUMAN",style: TextStyle(fontFamily: 'mars', color: Colors.black,fontSize: 70),),
              ),
              Container(
                alignment: Alignment.centerLeft,
                child: Text("You have decided to buy a puppy?",style: TextStyle(fontFamily: 'Comfortaa', color: Colors.grey,fontSize: 20),),
              ),
              Container(
                alignment: Alignment.centerLeft,
                child: Text("We help you to find your new friend",style: TextStyle(fontFamily: 'Comfortaa', color: Colors.grey,fontSize: 20),),
              ),
            ],
          ),
        ),

      ),
      //floating action to open container transform
      floatingActionButton: OpenContainer(
          closedBuilder: (_, openContainer){
            return FloatingActionButton(
              elevation: 0.0,
              onPressed: openContainer,
              backgroundColor: Colors.black,
              child: Icon(Icons.pets, color: Colors.white),
            );
          },
          openColor: Colors.blueGrey,
          closedElevation: 5.0,
          closedShape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(100)
          ),
          closedColor: Colors.blueGrey,
          //opening new screen here
          openBuilder: (_, closeContainer)
          {
            return Scaffold(
              appBar: AppBar(
                backgroundColor: Colors.black,
                title: Text("Dog Breed"),
                leading: IconButton(
                  onPressed: closeContainer,
                  icon: Icon(Icons.arrow_back, color: Colors.white),

                ),
              ),
              body: Scrollbar(
                  child: ListView(
                    padding: const EdgeInsets.symmetric(vertical: 8),
                    children: [
                      ListTile(
                        leading: ExcludeSemantics(
                          child: CircleAvatar(child: Text("A")),),
                        title: Text("Affenpinscher"),
                        subtitle: Text("Loyal, curious, and famously amusing"),
                        trailing: new Container(
                          child: Tooltip(
                            child: IconButton(icon: Icon(Icons.info,color: Colors.black,),
                                onPressed: (){ }),
                            message: 'Price: \$400',
                            padding: EdgeInsets.all(20),
                            margin: EdgeInsets.all(20),
                            showDuration: Duration(seconds: 2),
                            decoration: BoxDecoration(
                              color: Colors.black,
                              borderRadius: const BorderRadius.all(Radius.circular(4)),
                            ),
                            textStyle: TextStyle(color: Colors.white),
                            preferBelow: true,
                            verticalOffset: 200,
                          ),
                        ),
                       ),
                      ListTile(
                        leading: ExcludeSemantics(
                          child: CircleAvatar(child: Text("A")),),
                        title: Text("Airedale Terrier"),
                        subtitle: Text("The King of Terriers"),
                        trailing: new Container(
                          child: Tooltip(
                            child: IconButton(icon: Icon(Icons.info,color: Colors.black,),
                                onPressed: (){ }),
                            message: 'Price: \$450',
                            padding: EdgeInsets.all(20),
                            margin: EdgeInsets.all(20),
                            showDuration: Duration(seconds: 2),
                            decoration: BoxDecoration(
                              color: Colors.black,
                              borderRadius: const BorderRadius.all(Radius.circular(4)),
                            ),
                            textStyle: TextStyle(color: Colors.white),
                            preferBelow: true,
                            verticalOffset: 200,
                          ),
                        ),),
                      ListTile(
                        leading: ExcludeSemantics(
                          child: CircleAvatar(child: Text("B")),),
                        title: Text("Barbet"),
                        subtitle: Text("An archetypic water dog of France"),
                        trailing: new Container(
                          child: Tooltip(
                            child: IconButton(icon: Icon(Icons.info,color: Colors.black,),
                                onPressed: (){ }),
                            message: 'Price: \$500',
                            padding: EdgeInsets.all(20),
                            margin: EdgeInsets.all(20),
                            showDuration: Duration(seconds: 2),
                            decoration: BoxDecoration(
                              color: Colors.black,
                              borderRadius: const BorderRadius.all(Radius.circular(4)),
                            ),
                            textStyle: TextStyle(color: Colors.white),
                            preferBelow: true,
                            verticalOffset: 200,
                          ),
                        ),),
                      ListTile(
                        leading: ExcludeSemantics(
                          child: CircleAvatar(child: Text("B")),),
                        title: Text("Basenji"),
                        subtitle: Text("Africa’s Barkless Dog"),
                        trailing: new Container(
                          child: Tooltip(
                            child: IconButton(icon: Icon(Icons.info,color: Colors.black,),
                                onPressed: (){ }),
                            message: 'Price: \$350',
                            padding: EdgeInsets.all(20),
                            margin: EdgeInsets.all(20),
                            showDuration: Duration(seconds: 2),
                            decoration: BoxDecoration(
                              color: Colors.black,
                              borderRadius: const BorderRadius.all(Radius.circular(4)),
                            ),
                            textStyle: TextStyle(color: Colors.white),
                            preferBelow: true,
                            verticalOffset: 200,
                          ),
                        ),),
                      ListTile(
                        leading: ExcludeSemantics(
                          child: CircleAvatar(child: Text("C")),),
                        title: Text("Canaan Dog"),
                        subtitle: Text("The national dog of Israel"),
                        trailing: new Container(
                          child: Tooltip(
                            child: IconButton(icon: Icon(Icons.info,color: Colors.black,),
                                onPressed: (){ }),
                            message: 'Price: \$600',
                            padding: EdgeInsets.all(20),
                            margin: EdgeInsets.all(20),
                            showDuration: Duration(seconds: 2),
                            decoration: BoxDecoration(
                              color: Colors.black,
                              borderRadius: const BorderRadius.all(Radius.circular(4)),
                            ),
                            textStyle: TextStyle(color: Colors.white),
                            preferBelow: true,
                            verticalOffset: 200,
                          ),
                        ),),
                      ListTile(
                        leading: ExcludeSemantics(
                          child: CircleAvatar(child: Text("C")),
                        ),
                        title: Text("Cane Corso"),
                        subtitle: Text("Smart and Trainable"),
                        trailing: new Container(
                          child: Tooltip(
                            child: IconButton(icon: Icon(Icons.info,color: Colors.black,),
                                onPressed: (){ }),
                            message: 'Price: \$475',
                            padding: EdgeInsets.all(20),
                            margin: EdgeInsets.all(20),
                            showDuration: Duration(seconds: 2),
                            decoration: BoxDecoration(
                              color: Colors.black,
                              borderRadius: const BorderRadius.all(Radius.circular(4)),
                            ),
                            textStyle: TextStyle(color: Colors.white),
                            preferBelow: true,
                            verticalOffset: 200,
                          ),
                        ),),
                      ListTile(
                        leading: ExcludeSemantics(
                          child: CircleAvatar(child: Text("D")),
                        ),
                        title: Text("Doberman Pinscher"),
                        subtitle: Text("Sleek and Powerful"),
                        trailing: new Container(
                          child: Tooltip(
                            child: IconButton(icon: Icon(Icons.info,color: Colors.black,),
                                onPressed: (){ }),
                            message: 'Price: \$375',
                            padding: EdgeInsets.all(20),
                            margin: EdgeInsets.all(20),
                            showDuration: Duration(seconds: 2),
                            decoration: BoxDecoration(
                              color: Colors.black,
                              borderRadius: const BorderRadius.all(Radius.circular(4)),
                            ),
                            textStyle: TextStyle(color: Colors.white),
                            preferBelow: true,
                            verticalOffset: 200,
                          ),
                        ),),
                      ListTile(
                        leading: ExcludeSemantics(
                          child: CircleAvatar(child: Text("D")),
                        ),
                        title: Text("Dogo Argentino"),
                        subtitle: Text("A pack-hunting dog"),
                        trailing: new Container(
                          child: Tooltip(
                            child: IconButton(icon: Icon(Icons.info,color: Colors.black,),
                                onPressed: (){ }),
                            message: 'Price: \$425',
                            padding: EdgeInsets.all(20),
                            margin: EdgeInsets.all(20),
                            showDuration: Duration(seconds: 2),
                            decoration: BoxDecoration(
                              color: Colors.black,
                              borderRadius: const BorderRadius.all(Radius.circular(4)),
                            ),
                            textStyle: TextStyle(color: Colors.white),
                            preferBelow: true,
                            verticalOffset: 200,
                          ),
                        ),),
                      ListTile(
                        leading: ExcludeSemantics(
                          child: CircleAvatar(child: Text("E")),
                        ),
                        title: Text("Estrela Mountain Dog"),
                        subtitle: Text("An excellent livestock guardian"),
                        trailing: new Container(
                          child: Tooltip(
                            child: IconButton(icon: Icon(Icons.info,color: Colors.black,),
                                onPressed: (){ }),
                            message: 'Price: \$550',
                            padding: EdgeInsets.all(20),
                            margin: EdgeInsets.all(20),
                            showDuration: Duration(seconds: 2),
                            decoration: BoxDecoration(
                              color: Colors.black,
                              borderRadius: const BorderRadius.all(Radius.circular(4)),
                            ),
                            textStyle: TextStyle(color: Colors.white),
                            preferBelow: true,
                            verticalOffset: 200,
                          ),
                        ),),
                      ListTile(
                        leading: ExcludeSemantics(
                          child: CircleAvatar(child: Text("F")),
                        ),
                        title: Text("Field Spaniel"),
                        subtitle: Text("Sweet and Sensitive"),
                        trailing: new Container(
                          child: Tooltip(
                            child: IconButton(icon: Icon(Icons.info,color: Colors.black,),
                                onPressed: (){ }),
                            message: 'Price: \$400',
                            padding: EdgeInsets.all(20),
                            margin: EdgeInsets.all(20),
                            showDuration: Duration(seconds: 2),
                            decoration: BoxDecoration(
                              color: Colors.black,
                              borderRadius: const BorderRadius.all(Radius.circular(4)),
                            ),
                            textStyle: TextStyle(color: Colors.white),
                            preferBelow: true,
                            verticalOffset: 200,
                          ),
                        ),),
                      ListTile(
                        leading: ExcludeSemantics(
                          child: CircleAvatar(child: Text("F")),
                        ),
                        title: Text("Flat-Coated Retriever"),
                        subtitle: Text("Forever Young"),
                        trailing: new Container(
                          child: Tooltip(
                            child: IconButton(icon: Icon(Icons.info,color: Colors.black,),
                                onPressed: (){ }),
                            message: 'Price: \$525',
                            padding: EdgeInsets.all(20),
                            margin: EdgeInsets.all(20),
                            showDuration: Duration(seconds: 2),
                            decoration: BoxDecoration(
                              color: Colors.black,
                              borderRadius: const BorderRadius.all(Radius.circular(4)),
                            ),
                            textStyle: TextStyle(color: Colors.white),
                            preferBelow: true,
                            verticalOffset: 200,
                          ),
                        ),),
                    ],
                  ),
              ),
            );
          }
      ),
    );
  }
}